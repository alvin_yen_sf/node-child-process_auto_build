# node-child-process_auto_build

## 1. build one game project
- use `gdp` option
- `node . --gdp $absPathOfTheGameProject`
- i.e. `node . --gdp 'D:\sourcecode\igb-games\sideshow-slots'`
- get the tar file of built-game-project in the `builds` directory
    - i.e. `builds\sideshow-slots.tar`

## 2. build game projects
- create a csv file and add the game project paths to it
    - As the file extension is CSV, please remember to separate the paths by the comma.
    - please check example-game-list.csv if you need an example
- use `gdlcsv` option
- `node . --gdlcsv $absPathOfTheCsvFile`
- i.e. `node . --gdlcsv 'D:\playground\node-child-process_auto_build\example-game-list.csv'`
- get the tar file of built-game-projectsssss in the `root` directory of this project
    - i.e. `builds.tar`

## others
- check `example.sh` and `example-game-list.csv` for more information
