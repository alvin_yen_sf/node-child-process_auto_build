const shell = require('shelljs');
const argv = require('yargs').alias('gdp', 'game-directory-path').alias('gdlcsv', 'game-directory-list-csv-file').argv;
const path = require('path');
const fs = require('fs');
const csv = require('fast-csv');


const origPath = shell.pwd();
// console.log(`origPath: ${origPath}`); // \
// console.log();

let currentPath = shell.pwd();
// console.log(`currentPath: ${currentPath}`); // \
// console.log();

shell.rm('-rf', 'builds');
shell.mkdir('builds');

if (argv.gdp) {
    const argvGdpSplitArray = buildOneGame();

    // # move to the builds folder
    shell.cd(origPath + '\\builds'); // move to the builds folder
    // console.log(shell.pwd() + '');
    // console.log();

    // # tar the game folder
    const theCommand = `tar -zcvf ./${argvGdpSplitArray[argvGdpSplitArray.length - 1]}.tgz ./${argvGdpSplitArray[argvGdpSplitArray.length - 1]}`;
    // console.log(theCommand);
    // console.log();
    shell.exec(theCommand);

} else if (argv.gdlcsv) {
    // console.log(`argv.gdlcsv: ${argv.gdlcsv}`);
    buildGames();

} else {
    console.log('no argv.gdp or argv.gdlcsv');
    console.log();
}

console.log('complete the build-task successfully');
console.log();

function buildGames () {
    const argvGdlcsvSplitArray = splitThePath(argv.gdlcsv);
    // console.log(`argvGdlcsvSplitArray: ${argvGdlcsvSplitArray}`);
    // console.log();

    if (isCsvFile(argvGdlcsvSplitArray)) {
        getCsvContentArray(argvGdlcsvSplitArray).then(function (csvContent) {
            // console.log(`csvConent: ${csvContent}`);
            // console.log();

            // for (let i = 0; i < csvContent.length; i++) {
            //     buildOneGame(csvContent[i]);
            // }
            csvContent.forEach(gameProjectPath => {
                buildOneGame(gameProjectPath);
            });

            // # move to the builds folder
            shell.cd(origPath + '\\builds'); // move to the builds folder
            shell.cd('..'); // ### move to the upper folder
            // console.log(shell.pwd() + '');
            // console.log();

            // # tar the game folder
            const theCommand = 'tar -zcvf ./builds.tgz ./builds';
            // console.log(theCommand);
            // console.log();
            shell.exec(theCommand);

        }).catch(function () {
            console.log('read csv file failed');
            process.exit(1);
        });
    } else {
        console.log('need a csv file path to continue');
        process.exit(1);
    }
}

function getCsvContentArray (argvGdlcsvSplitArray) {
    return new Promise(function (resolve) {
        shell.cd(argvGdlcsvSplitArray[0]); // move to the partition
        fs.createReadStream(argv.gdlcsv)
            .pipe(csv())
            .on('data', function (data) {
                // console.log(data);
                // console.log();
                resolve(data);

            });
            // .on('end', function (data) {
            //     console.log('read finished');
            //     console.log();
            // });
    });
}

function isCsvFile (argvGdlcsvSplitArray) {
    const fileName = argvGdlcsvSplitArray[argvGdlcsvSplitArray.length - 1];
    const splitByDot = fileName.split('.');
    // console.log(`splitByDot: ${splitByDot}`);
    // console.log();
    return splitByDot[1] && splitByDot[1] === 'csv';
}

function splitThePath (raw) {
    return raw.split('\\');
}

function buildOneGame (thePath) {
    // console.log(argv.gdp);
    let argvGdpSplitArray = '';
    if (thePath) {
        argvGdpSplitArray = splitThePath(thePath); // windows path
    } else {
        argvGdpSplitArray = splitThePath(argv.gdp);
        // console.log(`argvGdpSplitArray: ${JSON.stringify(argvGdpSplitArray)}`);
        // console.log();
    }

    shell.cd(argvGdpSplitArray[0]); // move to the partition
    // currentPath = shell.pwd();
    // console.log(`currentPath: ${currentPath}`);
    // console.log();

    // # move to the path
    if (thePath) {
        shell.cd(thePath);
    } else {
        shell.cd(argv.gdp);
        // currentPath = shell.pwd();
        // console.log(`currentPath: ${currentPath}`);
        // console.log();
    }

    const buildsTarget = origPath+'\\builds\\'+argvGdpSplitArray[argvGdpSplitArray.length - 1];
    // console.log(buildsTarget);
    // console.log();

    const relativeBuildsTarget = path.relative(shell.pwd() + '', buildsTarget);
    // console.log(relativeBuildsTarget);
    // console.log();

    // # build the game project
    shell.exec(`igb-build --no-mangle -o ${relativeBuildsTarget}`);
    // shell.exec(`igb-build --no-mangle -o ~\\sourcecode\\igb-games\\tt`);
    // console.log();

    return argvGdpSplitArray;
}
